package iut.clermont.DroidJump.model.character;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.view.Display;

public class Ecran {
    private Bitmap imageDeLecran;
    private int ecranHauteur;
    private int ecranLargeur;


    public Ecran(Bitmap imageDeLecran, int ecranHauteur, int ecranLargeur) {
        this.imageDeLecran = imageDeLecran;
        this.ecranHauteur = ecranHauteur;
        this.ecranLargeur = ecranLargeur;

    }

    public Bitmap getImageDeLecran() {
        return imageDeLecran;
    }

    public void setImageDeLecran(Bitmap imageDeLecran) {
        this.imageDeLecran = imageDeLecran;
    }

    public int getEcranHauteur() {
        return ecranHauteur;
    }

    public int getEcranLargeur() {
        return ecranLargeur;
    }

    public void setEcranHauteur(int ecranHauteur) {
        this.ecranHauteur = ecranHauteur;
    }

    public void setEcranLargeur(int ecranLargeur) {
        this.ecranLargeur = ecranLargeur;
    }
}

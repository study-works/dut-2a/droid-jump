package iut.clermont.DroidJump.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.fragment.app.Fragment;

import java.util.List;

import iut.clermont.DroidJump.R;
import iut.clermont.DroidJump.database.DatabaseHandler;
import iut.clermont.DroidJump.model.Score;
import iut.clermont.DroidJump.view.adapter.ScoreAdapter;

public class ScoresFragment extends Fragment {
    private ListView scoresListView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_scores, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        scoresListView = view.findViewById(R.id.scores);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Context ctx = getContext();

        DatabaseHandler db = new DatabaseHandler(ctx);

        List<Score> scores = db.tenBestScores();

        if (scores != null) {
            scoresListView.setAdapter(new ScoreAdapter(ctx, android.R.layout.simple_list_item_1, scores));
        }

    }
}
















package iut.clermont.DroidJump.view.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.fragment.app.DialogFragment;

import iut.clermont.DroidJump.R;

public class DialogPseudoPopUpFragment extends DialogFragment {
    private Context ctx;

    private EditText pseudo;
    private Button apply;

    private MainFragmentCallBack parent;

    private SharedPreferences settings;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_dialog_pseudo, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        pseudo = view.findViewById(R.id.setPseudo);
        apply = view.findViewById(R.id.apply);

        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        ctx = getContext();

        settings = ctx.getSharedPreferences("SETTINGS", Context.MODE_PRIVATE);
        pseudo.setText(settings.getString("PSEUDO", null));

        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPseudo();
                getDialog().dismiss();
            }
        });

        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onPause() {
        setPseudo();

        super.onPause();
    }

    private void setPseudo() {
        settings = ctx.getSharedPreferences("SETTINGS", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        if (pseudo.getText().toString().equals("")) {
            editor.putString("PSEUDO", getString(R.string.anonymous));
        } else {
            editor.putString("PSEUDO", pseudo.getText().toString());
        }
        editor.commit();
        onItemSelected();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof MainFragmentCallBack) {
            parent = (MainFragmentCallBack) context;
        } else {
            throw new ClassCastException(context.toString()
                    + " must implement MainFragmentCallBack");
        }
    }

    /**
     * Call when user set its pseudo to indicate that the pesudo is set
     */
    private void onItemSelected() {
        parent.onItemSelected();
    }
}

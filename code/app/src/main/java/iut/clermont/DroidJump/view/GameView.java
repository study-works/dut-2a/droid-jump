package iut.clermont.DroidJump.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.Display;
import android.view.View;

import java.util.Random;

import iut.clermont.DroidJump.R;
import iut.clermont.DroidJump.model.character.Doodle;
import iut.clermont.DroidJump.model.character.Ecran;
import iut.clermont.DroidJump.model.character.Partie;
import iut.clermont.DroidJump.model.character.Plateforme;

public class GameView extends View {
    Handler handler;
    Runnable runnable;
    final int UPDATE_MILLIS=30;
    Partie partie;
    Doodle theDroid;
    int nombreDePlatformes = 8;
    Plateforme[] tableauDesPlateformesDeLaView = new Plateforme[nombreDePlatformes];
    Ecran ecran;
    Bitmap ecranBitmap,doodleBitmap;
    Bitmap plateformeBitmap;
    Display display;
    Point point;
    Rect rect;
    int velocity=0,gravity=3;
    int ecart=0;


    int scoreIncrement = 10;
    int score = 0;

    public GameView(Context context) {
        super(context);
    }

    public GameView(Context context, AttributeSet attrs) {
        super(context, attrs);
        handler = new Handler();
        partie = new Partie(false);
        display = ((Activity)getContext()).getWindowManager().getDefaultDisplay();
        point = new Point();
        display.getSize(point);
        runnable = new Runnable() {
            @Override
            public void run() {
                invalidate();
            }
        };
        doodleBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.droid);
        plateformeBitmap = BitmapFactory.decodeResource(getResources(),R.drawable.plateforme);
        ecranBitmap = BitmapFactory.decodeResource(getResources(),R.drawable.background);
        ecran = new Ecran(BitmapFactory.decodeResource(getResources(),R.drawable.background),point.y,point.x);
        for(int i=0;i<nombreDePlatformes;i++){
            tableauDesPlateformesDeLaView[i] = new Plateforme();
            tableauDesPlateformesDeLaView[i].setBitmapEntite(plateformeBitmap);
            tableauDesPlateformesDeLaView[i].setLargeur(plateformeBitmap.getWidth());
            tableauDesPlateformesDeLaView[i].setHauteur(plateformeBitmap.getHeight());
            tableauDesPlateformesDeLaView[i].sePlacerSurLAxeDesAbscissesAleatoirement(plateformeBitmap.getWidth(),ecran.getEcranLargeur());
            tableauDesPlateformesDeLaView[i].setPositionY(ecran.getEcranHauteur()-ecart);
            tableauDesPlateformesDeLaView[i].setEstMarquee(false);

            ecart+=225;


        }
        theDroid = new Doodle(tableauDesPlateformesDeLaView[5].getPositionX(),tableauDesPlateformesDeLaView[5].getPositionY()+20,doodleBitmap.getHeight(),doodleBitmap.getWidth(),doodleBitmap);
        rect = new Rect(0,0,ecran.getEcranLargeur(),ecran.getEcranHauteur());
    }



    @Override
    protected void onDraw(Canvas canvas){

       // System.out.println("Le droid se trouve en posY " +theDroid.getPositionY()+" et en posX : "+theDroid.getPositionX());

        super.onDraw(canvas);
        canvas.drawBitmap(ecranBitmap,null,rect,null);
        canvas.drawBitmap(theDroid.getBitmapEntite(),theDroid.getPositionX(), theDroid.getPositionY(),null);

        velocity = velocity + gravity;
        theDroid.setPositionY(theDroid.getPositionY()+velocity);
        if(theDroid.getPositionY()>ecran.getEcranHauteur()-100){
            partie.setGameOver(true);
        }
        for(int i=0;i<nombreDePlatformes;i++){
            canvas.drawBitmap(tableauDesPlateformesDeLaView[i].getBitmapEntite(),tableauDesPlateformesDeLaView[i].getPositionX(),tableauDesPlateformesDeLaView[i].getPositionY()-plateformeBitmap.getHeight(),null);
            if(theDroid.getPositionY()>=tableauDesPlateformesDeLaView[i].getPositionY()-plateformeBitmap.getHeight() && theDroid.getPositionY()<=tableauDesPlateformesDeLaView[i].getPositionY()-100 &&  theDroid.getPositionX()<tableauDesPlateformesDeLaView[i].getPositionX()+70 && theDroid.getPositionX()>=tableauDesPlateformesDeLaView[i].getPositionX()-70){//&& plateforme.getWidth()>=doodleX) {
                if(tableauDesPlateformesDeLaView[i].isEstMarquee()==false){
                    score+= scoreIncrement;
                   tableauDesPlateformesDeLaView[i].setEstMarquee(true);
                }
                velocity=-40;
                if(theDroid.getPositionY()<ecran.getEcranHauteur()-300) {
                    for (int k = 0; k < nombreDePlatformes;k++) {
                        tableauDesPlateformesDeLaView[k].setPositionY(tableauDesPlateformesDeLaView[k].getPositionY()+100);
                        if(tableauDesPlateformesDeLaView[k].getPositionY()>ecran.getEcranHauteur()){
                            tableauDesPlateformesDeLaView[k].setPositionY(0);
                            tableauDesPlateformesDeLaView[k].sePlacerSurLAxeDesAbscissesAleatoirement(plateformeBitmap.getWidth(),ecran.getEcranLargeur());
                            tableauDesPlateformesDeLaView[k].setEstMarquee(false);
                        }
                    }
                }
            }
        }
        handler.postDelayed(runnable,UPDATE_MILLIS);
    }

    public void setDoodleX(double value) {
       theDroid.seDeplacerSurLaxeDesAbscisses(value);
    }

    public boolean getIsGameOver() {
        return partie.isGameOver();
    }

    public int getScore() {
        return score;
    }

}

